package id.co.travels.wallo.activity.toko.produk;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.travels.wallo.R;
import id.co.travels.wallo.activity.profil.ProfilTokoActivity;
import id.co.travels.wallo.activity.toko.MenuActivity;
import id.co.travels.wallo.adapter.MenuAdapter;
import id.co.travels.wallo.adapter.ProdukAdapter;
import id.co.travels.wallo.api.BaseApiService;
import id.co.travels.wallo.api.UtilsApi;
import id.co.travels.wallo.model.data.Harga;
import id.co.travels.wallo.model.data.ItemProduk;
import id.co.travels.wallo.model.data.Kategori;
import id.co.travels.wallo.model.data.Menu;
import id.co.travels.wallo.model.data.Produk;
import id.co.travels.wallo.model.data.Shop;
import id.co.travels.wallo.model.response.ResponseKategori;
import id.co.travels.wallo.model.response.ResponseMenu;
import id.co.travels.wallo.model.response.ResponseProduk;
import id.co.travels.wallo.model.response.ResponseShop;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProdukActivity extends AppCompatActivity {

    @BindView(R.id.rv_produk)
    RecyclerView rvProduk;
    @BindView(R.id.sp_menu)
    Spinner spMenu;

    private ProdukAdapter mAdapter;
    List<ItemProduk> listProduk = new ArrayList<>();
    List<Menu> listMenu = new ArrayList<>();

    BaseApiService apiService;
    SharedPreferences pref;

    String idMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produk);
        ButterKnife.bind(this);

        pref = getSharedPreferences("setting", Activity.MODE_PRIVATE);
        apiService = UtilsApi.getAPIService();

        initToolbar();
        initRecyclerView();
    }

    public interface MyCallback {
        void onCallback(String value);
    }

    public interface KategoriCallback {
        void onCallback(List<Menu> value);
    }

    public interface SpinnerCallback {
        void onCallback(Spinner value);
    }

    private void initMenu(final Spinner spinner, final MyCallback myCallback, final KategoriCallback callback, final SpinnerCallback spCallback) {
        apiService.getMenuData(pref.getString("token", "")).enqueue(new Callback<ResponseMenu>() {
            @Override
            public void onResponse(Call<ResponseMenu> call, Response<ResponseMenu> response) {
                if (response.isSuccessful()) {
                    listMenu = response.body().getListMenu();
                    callback.onCallback(listMenu);

                    final List<String> menus = new ArrayList<>();
                    for (int i = 0; i < listMenu.size(); i++) {
                        menus.add(listMenu.get(i).getMenuName());
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ListProdukActivity.this, android.R.layout.simple_spinner_item, menus);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinner.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();

                    spCallback.onCallback(spinner);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String key = adapterView.getItemAtPosition(i).toString();
                            int index = menus.indexOf(key); //0, 1, 2
                            String id = listMenu.get(index).getIdMenu();
                            myCallback.onCallback(id);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseMenu> call, Throwable t) {
                Toast.makeText(ListProdukActivity.this, "Failed to Connect Internet !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        apiService.getProdukData(pref.getString("token", "")).enqueue(new Callback<ResponseProduk>() {
            @Override
            public void onResponse(Call<ResponseProduk> call, final Response<ResponseProduk> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        initMenu(spMenu, new MyCallback() {
                            @Override
                            public void onCallback(String value) {
                                idMenu = value;
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    Produk produkMenu = response.body().getData().get(i);
                                    Log.e("MENU", "" + produkMenu.getIdMenu() + " | " + idMenu);
                                    if (produkMenu.getIdMenu().equalsIgnoreCase(idMenu)) {
                                        listProduk = produkMenu.getItem();
                                        rvProduk.setAdapter(new ProdukAdapter(ListProdukActivity.this, listProduk));
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }, new KategoriCallback() {
                            @Override
                            public void onCallback(List<Menu> value) {
                                listMenu = value;
                            }
                        }, new SpinnerCallback() {
                            @Override
                            public void onCallback(Spinner value) {
                                spMenu = value;
                            }
                        });
                    } else {
                        Toast.makeText(ListProdukActivity.this, "Produk belum ada !", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to fetch data !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseProduk> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.btn_add)
    public void buttonAddProduk() {
        startActivity(new Intent(this, ProdukActivity.class));
    }

    private void initRecyclerView() {
        rvProduk.setHasFixedSize(true);
        rvProduk.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ProdukAdapter(this, listProduk);
        rvProduk.setAdapter(mAdapter);
    }

    private void initToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("List Produk");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
